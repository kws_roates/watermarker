﻿using ImageMagick;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Watermarker
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.InputEncoding = Encoding.Unicode;
            Console.OutputEncoding = Encoding.Unicode;

            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;

            List<string> users = new List<string>();

            if (!Directory.Exists(@".\images"))
            {
                Directory.CreateDirectory(@".\images");
            }

            string imagesdir = @".\images";

            //Excel format
            /*  Translator   |  Folder to Video Resource
             *  Hide         |  JP Videos
             *  Rob          |  EN Videos
             *  Jan          |  DE Videos
             * 
             * 
             */


            //load from excel file
            Console.WriteLine("Copy and Paste full path to Excel file with users names");
            string @userFile = Console.ReadLine();
            userFile = userFile.Replace("\"", "");

            Console.WriteLine("Copy and Paste OneDrive Path:");
            string @onedrivepath = Console.ReadLine();
            onedrivepath = onedrivepath.Replace("\"", "");

            ExcelPackage wb = new ExcelPackage(new FileInfo(userFile));
            foreach (ExcelWorksheet ws in wb.Workbook.Worksheets)
            {

                Console.WriteLine($"Tab: {ws.Name}");

                for (int rowx = 2; rowx < ws.Dimension.End.Row + 1; rowx++)
                {
                    if (ws.Cells[rowx, 1].Value != null && ws.Cells[rowx, 2].Value != null)
                    {
                        string username = ws.Cells[rowx, 1].Value.ToString().Trim();
                        
                        string videoDirectory = ws.Cells[rowx, 2].Value.ToString().Trim();
                        Console.WriteLine($"User: {username} -> {videoDirectory}");


                        string[] video_files = Directory.GetFiles(videoDirectory, "*.mp4", SearchOption.AllDirectories);

                        foreach (string pathToVideoFile in video_files)
                        {

                            string pathToImage = imagesdir;

                            int vid_height;
                            int vid_width;
                            using (MagickImage image_info = new MagickImage(pathToVideoFile))
                            {
                                vid_height = image_info.Height;
                                vid_width = image_info.Width;
                            }

                            Console.WriteLine($"{pathToVideoFile}\nheight: {vid_height} width: {vid_width}\n");


                            using (MagickImage image_text = new MagickImage())
                            {

                                MagickReadSettings settings = new MagickReadSettings()
                                {
                                    BackgroundColor = MagickColors.None,
                                    FillColor = MagickColors.None,
                                    StrokeColor = MagickColors.DarkGray,
                                    Font = "Arial",
                                    FontPointsize = 24,
                                    TextGravity = Gravity.Center,

                                };

                                image_text.Read($"caption:{username}", settings);
                                image_text.Rotate(-45.00);
                                image_text.Write(Path.Combine(pathToImage, $"{username}.png"));

                            }

                            //combine the images horizontally
                            using (MagickImageCollection image_out = new MagickImageCollection())
                            {

                                for (int col = 0; col < 30; col++)
                                {
                                    image_out.Add(Path.Combine(pathToImage, $"{username}.png"));
                                }

                                using (IMagickImage horizontal = image_out.AppendHorizontally())
                                {
                                    horizontal.Write(Path.Combine(pathToImage, $"{username}.png"));
                                }

                            }

                            //combine the images horizontally
                            using (MagickImageCollection image_out = new MagickImageCollection())
                            {

                                for (int row = 0; row < 20; row++)
                                {
                                    image_out.Add(Path.Combine(pathToImage, $"{username}.png"));
                                }

                                using (IMagickImage vertical = image_out.AppendVertically())
                                {
                                    vertical.Write(Path.Combine(pathToImage, $"{username}.png"));
                                }

                            }

                            //rotate image 45.0 degrees
                            using (MagickImage image_crop = new MagickImage(Path.Combine(pathToImage, $"{username}.png")))
                            {
                                image_crop.Crop(vid_width, vid_height);
                                image_crop.Write(Path.Combine(pathToImage, $"{username}.png"));
                            }

                            //combine image with video
                            string pathToNewVideo = pathToVideoFile.Replace(".mp4", $"_{username}.mp4");

                            Console.WriteLine($"Writing File {pathToNewVideo}");
                            string wmark = Path.Combine(pathToImage, $"{username}.png");
                            var ffMpeg = new NReco.VideoConverter.FFMpegConverter();

                            ffMpeg.FFMpegToolPath = @".\";

                            if (File.Exists(pathToNewVideo))
                            {
                                File.Delete(pathToNewVideo);
                            }

                            ffMpeg.Invoke($"-i \"{pathToVideoFile}\" -i \"{wmark}\" -filter_complex \"[0:0] overlay=0:0\" \"{pathToNewVideo}\"");

                            //once done move it to our new home on the cloud
                            string new_filename = new FileInfo(pathToNewVideo).Name;

                            if (!Directory.Exists(Path.Combine(onedrivepath, username)))
                            {
                                Directory.CreateDirectory(Path.Combine(onedrivepath, username));
                            }
                            File.Copy(pathToNewVideo, Path.Combine(onedrivepath, username, new_filename), true);

                            File.Delete(pathToNewVideo);

                            if (File.Exists(Path.Combine(pathToImage, $"{username}.png")))
                            {
                                File.Delete(Path.Combine(pathToImage, $"{username}.png"));
                            }

                        }
                    }
                }

            }



            Console.WriteLine("Done");
            Console.ReadLine();


        }
    }
}
